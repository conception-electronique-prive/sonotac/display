/**
 * @file    pin.h
 * @author  Paul Thomas
 * @date    5/29/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef STM32_CEP_SERVICES_PIN_H
#define STM32_CEP_SERVICES_PIN_H

#include "gpio.h"

struct Pin {
  GPIO_TypeDef*      port;
  uint32_t           pin;
  void               set() const { HAL_GPIO_WritePin(port, pin, GPIO_PIN_SET); }
  void               clear() const { HAL_GPIO_WritePin(port, pin, GPIO_PIN_RESET); }
  void               toggle() const { HAL_GPIO_TogglePin(port, pin); }
  void               write(bool state) const { HAL_GPIO_WritePin(port, pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET); }
  [[nodiscard]] bool get() const { return HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_SET; }
};

#endif  // STM32_CEP_SERVICES_PIN_H
