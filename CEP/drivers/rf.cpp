/**
 * @file    rf.cpp
 * @author  Paul Thomas
 * @date    6/7/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "rf.h"

#include <utility>

#include "services/logger.h"
#include "services/time.h"
#include "services/vector_utils.h"

#define TAG "RF"

RF* RF::sInstance = nullptr;

#define LOGD(msg, ...) STM_LOGD(TAG, msg __VA_OPT__(, ) __VA_ARGS__)
#define LOGI(msg, ...) STM_LOGI(TAG, msg __VA_OPT__(, ) __VA_ARGS__)
#define LOGW(msg, ...) STM_LOGW(TAG, msg __VA_OPT__(, ) __VA_ARGS__)
#define LOGE(msg, ...) STM_LOGE(TAG, msg __VA_OPT__(, ) __VA_ARGS__)

static bool check_result(const char* command, int result) {
  if (result != 0) {
    LOGE("%s failed: %x", command, result < 0 ? -result : result);
    return true;
  } else {
    LOGD("%s OK!", command);
    return false;
  }
}

RF::RF(SPI_HandleTypeDef* spi, const Pin& rst, const Pin& cs, const Pin& crdy, Callback callback) :
    mSpi(spi),
    mRst(rst),
    mCs(cs),
    mCrdy(crdy),
    mCallback(std::move(callback)) {
    stm_log_level_set(TAG, stm_log_level_t::STM_LOG_DEBUG);
}

bool RF::post() {
  mCs.set();
  mRst.set();
  return HAL_SPI_GetState(mSpi) == HAL_SPI_STATE_READY;
}

void RF::run() {
  if (mCrdy.get()) {
    mTxBuffer[0] = COMMAND_NOP;
    mCs.clear();
    HAL_SPI_TransmitReceive(mSpi, mTxBuffer.data(), mRxBuffer.data(), BUFFER_MAX_LENGTH, HAL_MAX_DELAY);
    mCs.set();
    LOGD(
        "RF received %s",
        u8_vector_to_hex_str({ mRxBuffer.begin(), mRxBuffer.end() }, print_buffer, print_buffer_length));
    if (mRxBuffer[0] != CODE_SUCCESS) return;
    if (mRxBuffer[1] != CODE_SUCCESS) return;
    mCallback({ mRxBuffer.begin() + CONTROLLER_STATUS_POS, mRxBuffer.end() });
  }
}

void RF::send(RF::Command command) {
  mTxBuffer[0] = command;
  mCs.clear();
  HAL_SPI_TransmitReceive(mSpi, mTxBuffer.data(), mRxBuffer.data(), 1, HAL_MAX_DELAY);
  mCs.set();
}
