/**
 * @file    rf.h
 * @author  Paul Thomas
 * @date    6/7/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef STM32_CEP_SERVICES_RF_RF_H
#define STM32_CEP_SERVICES_RF_RF_H

#include <spi.h>

#include <array>
#include <cstdint>
#include <deque>
#include <functional>
#include <vector>

#include "pin.h"

class RF {
 public:
  enum Command {
    COMMAND_NOP,
    COMMAND_STATUS,
    COMMAND_START,
    COMMAND_PREC,
    COMMAND_SUIV,
    COMMAND_VOLP,
    COMMAND_VOLM,
    COMMAND_GSSP,
    COMMAND_GSSM,
    COMMAND_PAIR,
    COMMAND_UNPAIR,
  };

  using Callback = std::function<void(const std::vector<uint8_t>&)>;

 public:
  RF(SPI_HandleTypeDef* spi, const Pin& rst, const Pin& cs, const Pin& crdy, Callback callback);

  static RF*  get() { return sInstance; }
  static void set(RF* instance) { sInstance = instance; }

  bool post();
  void run();
  void send(Command command);

 private:
  static RF*         sInstance;
  SPI_HandleTypeDef* mSpi;
  Pin                mRst;
  Pin                mCs;
  Pin                mCrdy;
  Callback           mCallback;

  static constexpr size_t  SPI_RESULT_POS           = 0;
  static constexpr size_t  SPI_RESULT_LENGTH        = 1;
  static constexpr size_t  CLIENT_RESULT_POS        = SPI_RESULT_POS + SPI_RESULT_LENGTH;
  static constexpr size_t  CLIENT_RESULT_LENGTH     = 1;
  static constexpr size_t  CONTROLLER_STATUS_POS    = CLIENT_RESULT_POS + CLIENT_RESULT_LENGTH;
  static constexpr size_t  CONTROLLER_STATUS_LENGTH = 7;
  static constexpr size_t  BUFFER_MAX_LENGTH        = CONTROLLER_STATUS_POS + CONTROLLER_STATUS_LENGTH;
  static constexpr uint8_t CODE_SUCCESS             = 0x55;

  std::array<uint8_t, BUFFER_MAX_LENGTH> mTxBuffer = { 0 };
  std::array<uint8_t, BUFFER_MAX_LENGTH> mRxBuffer = { 0 };
};

#endif  // STM32_CEP_SERVICES_RF_RF_H
