/**
 * @file    tlc5947.cpp
 * @author  Paul Thomas
 * @date    5/15/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "tlc5947.h"

#include <thread>

#include "services/logger.h"

void TLC5947::initialize(SPI_HandleTypeDef* handle, Pin blank, Pin xlat, uint8_t count) {
  if (count == 0) RuntimeException("Count cannot be 0");
  mHandle = handle;
  mBlank  = blank;
  setEnable(true);
  mXlat = xlat;
  mLeds.resize(sLedsPerDriver * count, 0);
  mTxBuf.resize(mLeds.size() + (mLeds.size() >> 1), 0);  // num leds * 1.5
  run();
  setEnable(false);
}

void TLC5947::run() {
  if (!mDirty) return;
  prepareTxBuf();
  auto r = HAL_SPI_Transmit(mHandle, mTxBuf.data(), mTxBuf.size(), HAL_MAX_DELAY);
  if (r != HAL_OK) {
    STM_LOGE(sTag, "Failed to transmit to LED. Reason: 0x%x", r);
    return;
  }

  mDirty = false;
  mXlat.write(true);
  mXlat.write(false);

  STM_LOGD(sTag, "Updated LED");
}

uint16_t TLC5947::getLed(Led::id_t index) { return mLeds[index]; }

void TLC5947::setLed(Led::id_t index, uint16_t value) {
  mDirty       = true;
  mLeds[index] = value;
  STM_LOGD(sTag, "Led %u = %u", index, mLeds[index]);
}

void TLC5947::setEnable(bool enabled) const { mBlank.write(enabled); }

[[maybe_unused]] double TLC5947::getMaxPct() const { return mMax; }

void TLC5947::setMaxPct(double pct) {
  if (pct < 0.0 || pct > 1.0) RuntimeException("Invalid value: Must be between 0 and 1");
  mMax = pct;
}

void TLC5947::prepareTxBuf() {
  std::size_t di = mTxBuf.size() - 1;
  for (std::size_t si = 0; si < mLeds.size(); si += 2) {
    // Here, we merge 2 12-bit block into 3 8-bit block
    // | ........   .... | ....   ........ |
    // | ........ | ....   .... | ........ |
    uint32_t recv  = (getClampedValue(si + 1) << 12) | getClampedValue(si);
    auto*    ptr   = reinterpret_cast<uint8_t*>(&recv);
    mTxBuf[di]     = ptr[0];
    mTxBuf[di - 1] = ptr[1];
    mTxBuf[di - 2] = ptr[2];
    di -= 3;
  }
}

uint16_t TLC5947::getClampedValue(Led::id_t index) {
  return static_cast<uint16_t>(static_cast<double>(mLeds[index]) * mMax);
}
