/**
 * @file    tlc5947.h
 * @author  Paul Thomas
 * @date    5/15/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MANETTE_MAIN_DRIVERS_TLC5947_H
#define MANETTE_MAIN_DRIVERS_TLC5947_H

#include <cstdint>
#include <vector>

#include "pin.h"
#include "services/led/interface.h"
#include "spi.h"

class TLC5947 : public Led::Interface {
 private:
  static constexpr std::size_t sLedsPerDriver = 24;
  static constexpr std::size_t sPwmResolution = 12;
  static constexpr std::size_t sTimeout       = 1000;

 public:
  static constexpr const char* sTag = "TLC";

 public:
  TLC5947() = default;
  void                               initialize(SPI_HandleTypeDef* handle, Pin blank, Pin xlat, uint8_t count = 1);
  void                               run();
  uint16_t                           getLed(Led::id_t index) override;
  void                               setLed(Led::id_t index, uint16_t value) override;
  [[maybe_unused, nodiscard]] double getMaxPct() const;
  void                               setMaxPct(double pct);
  void                               setEnable(bool enabled) const;

 private:
  void     prepareTxBuf();
  uint16_t getClampedValue(Led::id_t index);

 private:
  SPI_HandleTypeDef*    mHandle = nullptr;
  Pin                   mBlank  = {};
  Pin                   mXlat   = {};
  double                mMax    = Led::MaxValue;
  bool                  mDirty  = false;
  std::vector<uint8_t>  mTxBuf;
  std::vector<uint16_t> mLeds;
};

#endif  // MANETTE_MAIN_DRIVERS_TLC5947_H
