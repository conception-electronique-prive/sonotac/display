/**
 * @file    animator.cpp
 * @author  Paul Thomas
 * @date    5/16/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "animator.h"

#include "services/led/constants.h"
#include "services/logger.h"

void Animator::setInterface(Led::Interface* interface) { mInterface = interface; }

void Animator::run() {
  std::vector<id_t> ToErase;
  for (auto& [key, animation] : mAnimations) {
    if (!animation->isFinished()) {
      animation->run();
    } else if (animation->canRepeat()) {
      STM_LOGD(sTag, "Repeating animation");
      animation->repeat();
    } else {
      if (mAnimations[key]->exit()) ToErase.push_back(key);
    }
  }
  for (auto key : ToErase) {
    mAnimations.erase(key);
    STM_LOGD(sTag, "Removed completed animation on %lu", key);
  }
}

void Animator::clear() {
  if (!mAnimations.empty()) mAnimations.clear();
}
