/**
 * @file    animator.h
 * @author  Paul Thomas
 * @date    5/16/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MANETTE_MAIN_PROCESSES_ANIMATOR_H
#define MANETTE_MAIN_PROCESSES_ANIMATOR_H

#include <cstdint>
#include <functional>
#include <memory>
#include <unordered_map>
#include <vector>

#include "services/animation/animation.h"
#include "services/led/interface.h"
#include "services/logger.h"

class Animator {
 public:
  static constexpr const char* sTag = "Animator";

 public:
  using id_t = uint32_t;

  Animator() = default;
  void setInterface(Led::Interface* interface);
  void run();

  template <typename T>
  T* add(id_t id, std::unique_ptr<T> animation) {
    animation->mInterface = mInterface;
    mAnimations[id]       = std::move(animation);
    STM_LOGI(sTag, "Added new animation for %lu", id);
    mAnimations[id]->init();
    return static_cast<T*>(mAnimations[id].get());
  }

  void clear();

 private:
  Led::Interface*                                      mInterface = nullptr;
  std::unordered_map<id_t, std::unique_ptr<Animation>> mAnimations;
};

#endif  // MANETTE_MAIN_PROCESSES_ANIMATOR_H
