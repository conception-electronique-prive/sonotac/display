/**
 * @file    application.h.cpp
 * @author  Paul Thomas
 * @date    5/15/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "application.h"

#include "adc.h"
#include "animator.h"
#include "drivers/pin.h"
#include "drivers/rf.h"
#include "drivers/tlc5947.h"
#include "services/animation/filling_wave.h"
#include "services/animation/static.h"
#include "services/animation/wave.h"
#include "services/logger.h"
#include "spi.h"

namespace Application {

static constexpr const char* TAG   = "APP";
static volatile bool         doRun = true;
static TLC5947               tlc5947;
static Animator              animator;
static const Pin             BLANK = { LED_BLANK_GPIO_Port, LED_BLANK_Pin };
static const Pin             XLAT  = { LED_XLAT_GPIO_Port, LED_XLAT_Pin };

void setBreathIn();
void setHoldIn();
void setBreathOut();
void setHoldOut();

void setBreathIn() {
  animator
      .add(
          Animation::IdTop,
          std::make_unique<FillingWave>(Led::MaxValue, 0, 0, 5, std::chrono::seconds(3), Led::WaveLeds))
      ->setOnFinished([] {
        setHoldIn();
        return false;
      });
}

void setHoldIn() {
  animator.add(Animation::IdTop, std::make_unique<Static>(Led::MaxValue, std::chrono::seconds(2), Led::WaveLeds))
      ->setOnFinished([] {
        setBreathOut();
        return false;
      });
}

void setBreathOut() {
  animator
      .add(
          Animation::IdTop,
          std::make_unique<FillingWave>(0, Led::MaxValue, 0, 5, std::chrono::seconds(3), Led::WaveLedsInv))
      ->setOnFinished([] {
        setBreathIn();
        return false;
      });
}

void init() {
  //  stm_log_level_set(Animator::sTag, STM_LOG_DEBUG);
  //  stm_log_level_set(TLC5947::sTag, STM_LOG_DEBUG);
  tlc5947.initialize(&hspi1, BLANK, XLAT);
  tlc5947.setMaxPct(1.0);
  animator.setInterface(&tlc5947);
  setBreathIn();
  //  animator.add(Animation::IdBottom, std::make_unique<Static>(Led::MaxValue, std::chrono::seconds(10),
  //  Led::ModeLeds))
  //      ->setRepeat(Animation::REPEAT_UNLIMITED);
}

void run() {
  while (doRun) {
    animator.run();
    tlc5947.run();

    // Required to avoid flickering on the LED
    // For some reason, if we refresh the LED too often
    // The LED will slightly flicker even though they should not
    // My guess is that the LED driver cannot update the LED too frequently
    HAL_Delay(10);
  }
}

}  // namespace Application
