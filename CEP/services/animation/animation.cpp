/**
 * @file    animation.cpp
 * @author  Paul Thomas
 * @date    5/16/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "animation.h"

#include <chrono>

#include "services/led/constants.h"
#include "services/time.h"
#include "stm32g0xx_hal.h"

Animation::Animation(std::chrono::duration<uint32_t, std::milli> duration, const std::vector<Led::id_t>& leds) :
    mStart(HAL_GetTick()),
    mEnd(mStart + duration),
    mLeds(leds) { }

Animation& Animation::setRepeat(int32_t repeat) {
  mRepeat = repeat;
  return *this;
}

Animation& Animation::setOnFinished(const std::function<bool()>& callback) {
  mCallback = callback;
  return *this;
}

void Animation::init() { }

void Animation::run() { }

bool Animation::exit() { return mCallback(); }

bool Animation::isFinished() const {
  if (mEnd.count() == 0) return false;
  return getNow() > mEnd;
}

bool Animation::canRepeat() const { return mRepeat != 0; }

void Animation::repeat() {
  auto duration = mEnd - mStart;
  mStart        = getNow();
  mEnd          = mStart + duration;
  if (mRepeat > 0) --mRepeat;
}

double Animation::getProgress() const {
  return static_cast<double>((getNow() - mStart).count()) / static_cast<double>((mEnd - mStart).count());
}
