/**
 * @file    animation.h
 * @author  Paul Thomas
 * @date    5/16/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MANETTE_MAIN_SERVICES_ANIMATIONS_BASE_H
#define MANETTE_MAIN_SERVICES_ANIMATIONS_BASE_H

#include <chrono>
#include <cstdint>
#include <functional>
#include <vector>

#include "services/led/constants.h"
#include "services/led/interface.h"

class Animator;

class Animation {
 public:
  static constexpr const char* sTag = "Anim";

  static constexpr int32_t REPEAT_UNLIMITED = -1;

  static constexpr uint32_t IdTop    = 1;
  static constexpr uint32_t IdBottom = 2;
  static constexpr uint32_t IdAll    = 3;

 public:
  Animation(std::chrono::duration<uint32_t, std::milli> duration, const std::vector<Led::id_t>& leds);
  Animation(const Animation&) = default;
  virtual ~Animation()        = default;
  Animation& setRepeat(int32_t repeat);
  Animation& setOnFinished(const std::function<bool(void)>& callback);

 protected:
  virtual void         init();
  virtual void         run();
  virtual bool         exit();
  [[nodiscard]] bool   isFinished() const;
  [[nodiscard]] bool   canRepeat() const;
  virtual void         repeat();
  [[nodiscard]] double getProgress() const;

 protected:
  friend class ::Animator;
  std::chrono::duration<uint32_t, std::milli> mStart;
  std::chrono::duration<uint32_t, std::milli> mEnd;
  int32_t                                     mRepeat    = 0;
  std::vector<Led::id_t>                      mLeds      = {};
  Led::Interface*                             mInterface = nullptr;
  std::function<bool(void)>                   mCallback  = [] { return true; };
};

#endif  // MANETTE_MAIN_SERVICES_ANIMATIONS_BASE_H
