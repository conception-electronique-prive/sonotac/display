/**
 * @file    fade_out.cpp
 * @author  Paul Thomas
 * @date    5/24/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "fade_out.h"

#include "services/logger.h"

static constexpr const char* tag = "FadeOut";

FadeOut::FadeOut(std::chrono::duration<uint32_t, std::milli> duration, const std::vector<Led::id_t>& leds) :
    Animation(duration, leds) {
  //    stm_log_level_set(tag, STM_LOG_DEBUG);
  mInitialValues.reserve(mLeds.size());
}

FadeOut* FadeOut::setInitialValue(uint16_t value) {
  mInitialValues.clear();
  mInitialValues.resize(mLeds.size(), value);
  STM_LOGD(tag, "FadeOut - setInitialValue -> LED(1) = %d", mInitialValues[0]);
  return this;
}

void FadeOut::init() {
  if (!mInitialValues.empty()) return;
  for (auto id : mLeds) {
    mInitialValues.push_back(mInterface->getLed(id));
    STM_LOGD(tag, "FadeOut - init -> LED(%d) = %d", id, mInterface->getLed(id));
  }
}

void FadeOut::run() {
  auto progress = getProgress();
  auto f        = [progress](uint32_t value) { return static_cast<uint32_t>(value * (1.0f - progress)); };
  for (std::size_t i = 0; i < mInitialValues.size(); ++i) { mInterface->setLed(mLeds[i], f(mInitialValues[i])); }
  //    STM_LOGD(tag, "FadeOut - Run -> LED(1) = %lu", f(m_initialValues[0]));
}

bool FadeOut::exit() {
  for (std::size_t i = 0; i < mInitialValues.size(); ++i) { mInterface->setLed(mLeds[i], 0); }
  return Animation::exit();
  //    esp_log_level_set(tag, STM_LOG_INFO);
}
