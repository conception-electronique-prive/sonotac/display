/**
 * @file    fade_out.h
 * @author  Paul Thomas
 * @date    5/24/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MANETTE_MAIN_SERVICES_ANIMATIONS_FADE_OUT_H
#define MANETTE_MAIN_SERVICES_ANIMATIONS_FADE_OUT_H

#include "animation.h"

class FadeOut : public Animation {
 public:
  FadeOut(std::chrono::duration<uint32_t, std::milli> duration, const std::vector<Led::id_t>& leds);
  FadeOut* setInitialValue(uint16_t value);

 protected:
  void init() final;
  void run() final;
  bool exit() final;

 private:
  std::vector<uint16_t> mInitialValues;
};

#endif  // MANETTE_MAIN_SERVICES_ANIMATIONS_FADE_OUT_H
