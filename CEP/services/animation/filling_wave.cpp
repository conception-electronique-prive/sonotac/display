/**
 * @file    filling_wave.cpp
 * @author  Paul Thomas
 * @date    2023-09-25
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "filling_wave.h"

#include "services/logger.h"
#include "services/time.h"

FillingWave::FillingWave(
    uint16_t                                    inactiveLevel,
    uint16_t                                    activeLevel,
    uint8_t                                     fillStart,
    uint32_t                                    width,
    std::chrono::duration<uint32_t, std::milli> duration,
    const std::vector<Led::id_t>               &leds) :
    Animation(duration, leds) {
  mRanges.reserve(mLeds.size());

  auto quantum = duration.count() / (mLeds.size() + width * 2);
  for (uint32_t i = 0; i < mLeds.size(); ++i) {
    mRanges[i] = Range(activeLevel, inactiveLevel, i, width, quantum, i >= fillStart);
  }
}

void FillingWave::run() {
  auto progress = (getNow() - mStart).count();
  for (uint32_t i = 0; i < mLeds.size(); ++i) {
    auto expected = mRanges[i].getLevel(progress);
    if (i == 8)
      STM_LOGD(
          sTag,
          "%d: %3.2f%% -> %d",
          i,
          (static_cast<double>(progress) / static_cast<double>((mEnd - mStart).count()) * 100),
          expected);
    mInterface->setLed(mLeds[i], expected);
  }
}

FillingWave::Range::Range(
    uint16_t inactiveLevel,
    uint16_t activeLevel,
    uint32_t index,
    uint32_t width,
    uint32_t quantum,
    bool     persistent) :
    mInactiveLevel(inactiveLevel),
    mActiveLevel(activeLevel),
    mPersistent(persistent) {
  mCenter = quantum * (width + index);
  mLeft   = quantum * index;
  mRight  = quantum * (width * 2 + index);
}

uint16_t FillingWave::Range::getLevel(uint32_t progress) {
  if (progress < mLeft) return mInactiveLevel;
  if (mCenter < progress && mPersistent) return mActiveLevel;
  if (mRight < progress) return mInactiveLevel;

  auto diff  = progress > mCenter ? progress - mCenter : mCenter - progress;
  auto ratio = 1 - (static_cast<double>(diff) / static_cast<double>(mCenter - mLeft));
  mValue     = mActiveLevel * ratio + mInactiveLevel * (1 - ratio);
  STM_LOGD(sTag, "%1.4f -> %3.2f", ratio, mValue);
  return static_cast<uint16_t>(mValue);
}
