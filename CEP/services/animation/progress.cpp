/**
 * @file    progress.cpp
 * @author  Paul Thomas
 * @date    5/16/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "progress.h"

Progress::Progress(
    uint32_t                                    value,
    uint32_t                                    max,
    std::chrono::duration<uint32_t, std::milli> duration,
    const std::vector<Led::id_t>&               leds) :
    Animation(duration, leds) {
  mStates.resize(leds.size());
  auto quantum = max / leds.size();
  for (std::size_t i = 0; i < leds.size(); ++i) { mStates[i] = (quantum * i) < value; }
}

void Progress::init() {
  for (std::size_t i = 0; i < mLeds.size(); ++i) { mInterface->setLed(mLeds[i], mStates[i] ? Led::MaxValue : 0); }
}
