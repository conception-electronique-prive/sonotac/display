/**
 * @file    wave.cpp
 * @author  Paul Thomas
 * @date    5/16/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "wave.h"

#include <cmath>

#include "services/logger.h"
#include "services/time.h"

Wave::Wave(uint32_t width, std::chrono::duration<uint32_t, std::milli> duration, const std::vector<Led::id_t>& leds) :
    Animation(duration, leds) {
  mRanges.reserve(mLeds.size());
  auto quantum = duration.count() / (mLeds.size() + width * 2);
  for (uint32_t i = 0; i < mLeds.size(); i++) { mRanges[i] = Range(i, width, quantum); }
}

void Wave::run() {
  auto progress = (getNow() - mStart).count();
  bool first    = true;
  for (uint32_t i = 0; i < mLeds.size(); ++i) {
    auto expected = mRanges[i].getLevel(progress);
    if (first) {
      first = false;
      STM_LOGD(
          sTag,
          "%3.2f%% -> %d",
          (static_cast<double>(progress) / static_cast<double>((mEnd - mStart).count()) * 100),
          expected);
    }
    mInterface->setLed(mLeds[i], expected);
  }
}

Wave::Range::Range(uint32_t index, uint32_t width, uint32_t quantum) {
  mCenter = quantum * (width + index);
  mLeft   = quantum * index;
  mRight  = quantum * (width * 2 + index);
}

uint16_t Wave::Range::getLevel(uint32_t progress) const {
  if (progress < mLeft || mRight < progress) return 0;
  auto diff  = progress > mCenter ? progress - mCenter : mCenter - progress;
  auto ratio = 1 - (static_cast<double>(diff) / static_cast<double>(mCenter - mLeft));
  auto out   = sin(ratio * M_PI_2);
  STM_LOGD(sTag, "%1.4f -> %3.2f", ratio, out);
  return static_cast<uint16_t>(out * Led::MaxValue);
}
