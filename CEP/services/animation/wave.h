/**
 * @file    wave.h
 * @author  Paul Thomas
 * @date    5/16/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MANETTE_MAIN_SERVICES_ANIMATIONS_WAVE_H
#define MANETTE_MAIN_SERVICES_ANIMATIONS_WAVE_H

#include "animation.h"

class Wave : public Animation {
 private:
  class Range {
   public:
    Range(uint32_t index, uint32_t width, uint32_t quantum);
    [[nodiscard]] uint16_t getLevel(uint32_t progress) const;

   private:
    uint64_t mCenter;
    uint64_t mLeft;
    uint64_t mRight;
  };

 public:
  Wave(uint32_t width, std::chrono::duration<uint32_t, std::milli> duration, const std::vector<Led::id_t>& leds);
  Wave(const Wave&) = default;
  ~Wave() override  = default;

 protected:
  void run() override;

 private:
  static constexpr const char* sTag = "Wave";
  std::vector<Range>           mRanges;
};

#endif  // MANETTE_MAIN_SERVICES_ANIMATIONS_WAVE_H
