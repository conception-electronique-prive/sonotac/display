/**
 * @file    constants.cpp
 * @author  Paul Thomas
 * @date    5/25/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "constants.h"

#include <vector>

namespace Led {

const std::vector<id_t> WaveLeds = { D19, D18, D17, D16, D15, D14, D13, D12, D11, D10, D09, D08 };

const std::vector<id_t> WaveLedsInv = { D08, D09, D10, D11, D12, D13, D14, D15, D16, D17, D18, D19 };

const std::vector<id_t> ModeLeds = { D20, D21, D22, D23, D24, D25, D26, D27, D28, D29, D30, D31 };

const std::vector<id_t> ModeLedsInv = { D31, D30, D29, D28, D27, D26, D25, D24, D23, D22, D21, D20 };

const std::vector<id_t> AllLeds = { D19, D18, D17, D16, D15, D14, D13, D12, D11, D10, D09, D08,
                                    D20, D21, D22, D23, D24, D25, D26, D27, D28, D29, D30, D31 };

const std::vector<id_t> AllLedsInv = { D08, D09, D10, D11, D12, D13, D14, D15, D16, D17, D18, D19,
                                       D31, D30, D29, D28, D27, D26, D25, D24, D23, D22, D21, D20 };
}  // namespace Led
