/**
 * @file    constant.h
 * @author  Paul Thomas
 * @date    5/17/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MANETTE_MAIN_SERVICES_LED_CONSTANT_H
#define MANETTE_MAIN_SERVICES_LED_CONSTANT_H

#include <cstdint>
#include <vector>

namespace Led {
using id_t                         = uint16_t;
static constexpr uint16_t MaxValue = 4095;

static constexpr id_t D08 = 0;
static constexpr id_t D09 = 1;
static constexpr id_t D10 = 2;
static constexpr id_t D11 = 3;
static constexpr id_t D12 = 4;
static constexpr id_t D13 = 5;
static constexpr id_t D14 = 6;
static constexpr id_t D15 = 7;
static constexpr id_t D16 = 8;
static constexpr id_t D17 = 9;
static constexpr id_t D18 = 10;
static constexpr id_t D19 = 11;
static constexpr id_t D20 = 12;
static constexpr id_t D21 = 13;
static constexpr id_t D22 = 14;
static constexpr id_t D23 = 15;
static constexpr id_t D24 = 16;
static constexpr id_t D25 = 17;
static constexpr id_t D26 = 18;
static constexpr id_t D27 = 19;
static constexpr id_t D28 = 20;
static constexpr id_t D29 = 21;
static constexpr id_t D30 = 22;
static constexpr id_t D31 = 23;

extern const std::vector<id_t> WaveLeds;
extern const std::vector<id_t> WaveLedsInv;
extern const std::vector<id_t> ModeLeds;
extern const std::vector<id_t> ModeLedsInv;
extern const std::vector<id_t> AllLeds;
extern const std::vector<id_t> AllLedsInv;

}  // namespace Led

#endif  // MANETTE_MAIN_SERVICES_LED_CONSTANT_H
