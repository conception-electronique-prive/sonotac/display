/**
 * @file    led_interface.h
 * @author  Paul Thomas
 * @date    5/16/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MANETTE_MAIN_SERVICES_LED_INTERFACE_H
#define MANETTE_MAIN_SERVICES_LED_INTERFACE_H

#include <cstdint>

#include "constants.h"

namespace Led {
class Interface {
 public:
  virtual uint16_t getLed(id_t index)                 = 0;
  virtual void     setLed(id_t index, uint16_t value) = 0;
};
}  // namespace Led

#endif  // MANETTE_MAIN_SERVICES_LED_INTERFACE_H
