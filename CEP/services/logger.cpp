/**
 * @file    logger.cpp
 * @author  Paul Thomas
 * @date    8/11/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

/**
 * @file    debug.cpp
 * @author  Paul Thomas
 * @date    5/29/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "logger.h"

#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <map>
#include <string>

#include "main.h"

static stm_log_level_t                           s_stm_log_default_level = STM_LOG_INFO;
static std::map<std::string, stm_log_level_t>    s_stm_log_levels;
static std::function<void(const char*, va_list)> s_stm_log_function = [](const char* format, va_list args) {
  vprintf(format, args);
};

void stm_log_function_set(std::function<void(const char*, va_list)> func) { s_stm_log_function = func; }

void stm_log_level_set(const char* tag, stm_log_level_t level) {
  if (strcmp(tag, "*") == 0) {
    s_stm_log_default_level = level;
  } else {
    s_stm_log_levels[std::string(tag)] = level;
  }
}

void stm_log_write(stm_log_level_t level, const char* tag, const char* format, ...) {
  auto tag_level_position = s_stm_log_levels.find(tag);
  auto tag_level          = s_stm_log_default_level;
  if (tag_level_position != s_stm_log_levels.end()) tag_level = tag_level_position->second;
  if (level > tag_level) return;

  va_list list;
  va_start(list, format);
  s_stm_log_function(format, list);
  va_end(list);
}

uint32_t stm_log_timestamp() { return HAL_GetTick(); }
