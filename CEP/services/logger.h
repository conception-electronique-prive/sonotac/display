/**
 * @file    logger.h
 * @author  Paul Thomas
 * @date    8/11/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MATRIUS_SENSOR_SRC_CEP_SERVICES_LOGGER_H
#define MATRIUS_SENSOR_SRC_CEP_SERVICES_LOGGER_H

/**
 * @file    debug.h
 * @author  Paul Thomas
 * @date    5/29/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef STM32_CEP_SERVICES_DEBUG_H
#define STM32_CEP_SERVICES_DEBUG_H

#include <stdint.h>

#include <cstdarg>
#include <functional>

typedef enum {
  STM_LOG_NONE,   /*!< No log output */
  STM_LOG_ERROR,  /*!< Critical errors, software module can not recover on its own */
  STM_LOG_WARN,   /*!< Error conditions from which recovery measures have been taken */
  STM_LOG_INFO,   /*!< Information messages which describe normal flow of events */
  STM_LOG_DEBUG,  /*!< Extra information which is not necessary for normal use (values, pointers, sizes, etc). */
  STM_LOG_VERBOSE /*!< Bigger chunks of debugging information, or frequent messages which can potentially flood the
                   output. */
} stm_log_level_t;

void stm_log_function_set(std::function<void(const char*, va_list)> function);

void stm_log_level_set(const char* tag, stm_log_level_t level);

void stm_log_write(stm_log_level_t level, const char* tag, const char* format, ...);

uint32_t stm_log_timestamp();

#define LOG_COLOR_BLACK  "30"
#define LOG_COLOR_RED    "31"
#define LOG_COLOR_GREEN  "32"
#define LOG_COLOR_BROWN  "33"
#define LOG_COLOR_BLUE   "34"
#define LOG_COLOR_PURPLE "35"
#define LOG_COLOR_CYAN   "36"
#define LOG_COLOR(COLOR) "\033[0;" COLOR "m"
#define LOG_BOLD(COLOR)  "\033[1;" COLOR "m"
#define LOG_RESET_COLOR  "\033[0m"
#define LOG_COLOR_E      LOG_COLOR(LOG_COLOR_RED)
#define LOG_COLOR_W      LOG_COLOR(LOG_COLOR_BROWN)
#define LOG_COLOR_I      LOG_COLOR(LOG_COLOR_GREEN)
#define LOG_COLOR_D
#define LOG_COLOR_V
#define LOG_BELL_E "\a"
#define LOG_BELL_W "\a"
#define LOG_BELL_I
#define LOG_BELL_D
#define LOG_BELL_V

#define LOG_FORMAT(letter, format)                                                                                     \
  LOG_BELL_##letter LOG_COLOR_##letter #letter " (%010d) [%-7s]: " format LOG_RESET_COLOR "\r\n"

#define STM_LOG_LEVEL(level, tag, fmt, ...)                                                                            \
  do {                                                                                                                 \
    if (level == STM_LOG_ERROR) {                                                                                      \
      stm_log_write(STM_LOG_ERROR, tag, LOG_FORMAT(E, fmt), stm_log_timestamp(), tag __VA_OPT__(, ) __VA_ARGS__);      \
    } else if (level == STM_LOG_WARN) {                                                                                \
      stm_log_write(STM_LOG_WARN, tag, LOG_FORMAT(W, fmt), stm_log_timestamp(), tag __VA_OPT__(, ) __VA_ARGS__);       \
    } else if (level == STM_LOG_INFO) {                                                                                \
      stm_log_write(STM_LOG_INFO, tag, LOG_FORMAT(I, fmt), stm_log_timestamp(), tag __VA_OPT__(, ) __VA_ARGS__);       \
    } else if (level == STM_LOG_DEBUG) {                                                                               \
      stm_log_write(STM_LOG_DEBUG, tag, LOG_FORMAT(D, fmt), stm_log_timestamp(), tag __VA_OPT__(, ) __VA_ARGS__);      \
    }                                                                                                                  \
  } while (0)

#define STM_LOGE(tag, fmt, ...) STM_LOG_LEVEL(STM_LOG_ERROR, tag, fmt __VA_OPT__(, ) __VA_ARGS__)
#define STM_LOGW(tag, fmt, ...) STM_LOG_LEVEL(STM_LOG_WARN, tag, fmt __VA_OPT__(, ) __VA_ARGS__)
#define STM_LOGI(tag, fmt, ...) STM_LOG_LEVEL(STM_LOG_INFO, tag, fmt __VA_OPT__(, ) __VA_ARGS__)
#define STM_LOGD(tag, fmt, ...) STM_LOG_LEVEL(STM_LOG_DEBUG, tag, fmt __VA_OPT__(, ) __VA_ARGS__)

#endif  // STM32_CEP_SERVICES_DEBUG_H

#endif  // MATRIUS_SENSOR_SRC_CEP_SERVICES_LOGGER_H
