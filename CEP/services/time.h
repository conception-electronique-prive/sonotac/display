/**
 * @file    time.h
 * @author  Paul Thomas
 * @date    9/18/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MANETTE_STM32_CEP_SERVICES_TIME_H
#define MANETTE_STM32_CEP_SERVICES_TIME_H

#include <chrono>
#include <cstdint>

std::chrono::duration<uint32_t, std::milli> getNow();
std::chrono::duration<uint32_t, std::milli> makeTimeout(std::chrono::duration<uint32_t, std::milli> duration);

#endif  // MANETTE_STM32_CEP_SERVICES_TIME_H
