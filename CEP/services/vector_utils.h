/**
 * @file    vector_utils.h
 * @author  Paul Thomas
 * @date    2023-10-02
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef MANETTE_STM32_CEP_SERVICES_VECTOR_UTILS_H
#define MANETTE_STM32_CEP_SERVICES_VECTOR_UTILS_H

#include <cstddef>
#include <cstdint>
#include <vector>

static constexpr size_t print_buffer_length = 50;
extern char print_buffer[print_buffer_length];
const char* u8_vector_to_hex_str(const std::vector<uint8_t>& v, char* buffer, size_t length);

#endif  // MANETTE_STM32_CEP_SERVICES_VECTOR_UTILS_H
