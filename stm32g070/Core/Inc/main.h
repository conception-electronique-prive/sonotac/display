/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void SystemClock_Config();
void RuntimeException(const char* message, ...);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define START_Pin GPIO_PIN_0
#define START_GPIO_Port GPIOA
#define GSSM_Pin GPIO_PIN_1
#define GSSM_GPIO_Port GPIOA
#define GSSP_Pin GPIO_PIN_2
#define GSSP_GPIO_Port GPIOA
#define PREC_Pin GPIO_PIN_3
#define PREC_GPIO_Port GPIOA
#define SUIV_Pin GPIO_PIN_4
#define SUIV_GPIO_Port GPIOA
#define VOLM_Pin GPIO_PIN_5
#define VOLM_GPIO_Port GPIOA
#define VOLP_Pin GPIO_PIN_6
#define VOLP_GPIO_Port GPIOA
#define VBAT_READ_Pin GPIO_PIN_7
#define VBAT_READ_GPIO_Port GPIOA
#define RF_RST_Pin GPIO_PIN_0
#define RF_RST_GPIO_Port GPIOB
#define RF_CRDY_Pin GPIO_PIN_1
#define RF_CRDY_GPIO_Port GPIOB
#define RF_MISO_Pin GPIO_PIN_2
#define RF_MISO_GPIO_Port GPIOB
#define RF_SCK_Pin GPIO_PIN_10
#define RF_SCK_GPIO_Port GPIOB
#define RF_MOSI_Pin GPIO_PIN_11
#define RF_MOSI_GPIO_Port GPIOB
#define RF_CS_Pin GPIO_PIN_12
#define RF_CS_GPIO_Port GPIOB
#define LATCH_Pin GPIO_PIN_13
#define LATCH_GPIO_Port GPIOB
#define LED_BLANK_Pin GPIO_PIN_6
#define LED_BLANK_GPIO_Port GPIOC
#define LED_XLAT_Pin GPIO_PIN_7
#define LED_XLAT_GPIO_Port GPIOC
#define LED_MISO_Pin GPIO_PIN_11
#define LED_MISO_GPIO_Port GPIOA
#define LED_MOSI_Pin GPIO_PIN_12
#define LED_MOSI_GPIO_Port GPIOA
#define LED_SCK_Pin GPIO_PIN_3
#define LED_SCK_GPIO_Port GPIOB
#define DEBUG_TX_Pin GPIO_PIN_6
#define DEBUG_TX_GPIO_Port GPIOB
#define DEBUG_RX_Pin GPIO_PIN_7
#define DEBUG_RX_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
